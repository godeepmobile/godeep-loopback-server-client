window.App = Ember.Application.create();

App.ApplicationAdapter = DS.RESTAdapter.extend({
	'namespace' : 'api'
  , 'host'      : 'http://localhost:3000'
});

App.ApplicationSerializer = DS.RESTSerializer.extend({
    normalizePayload: function(payload) {
       	var organizations     = payload.Organizations
       	  , organizationTypes = []
       	  , conferences       = []
       	  , i                 = organizations.length - 1;

       	for (i ; i >= 0; i--) {
       		organizationTypes.push(organizations[i].goOrganizationType);
       		conferences.push(organizations[i].goConference);
       	}

       	payload['OrganizationTypes'] = organizationTypes;
       	payload['Conferences'] = conferences;

        return payload;
    },

    extractArray: function(store, type, payload) {
    	return this._super(store, type, payload);
    }
});