App.Router.map(function() {
	this.route('organizations', {
		'path' : '/'
	});
});

App.OrganizationsRoute = Ember.Route.extend({
	'model' : function() {
		return this.store.find('organization');
	}
});

