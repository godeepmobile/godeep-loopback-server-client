App.Conference = DS.Model.extend({
	'name'             : DS.attr('string')
  , 'description'      : DS.attr('string')
  , 'organizations'    : DS.hasMany('organization')
  , 'organizationType' : DS.belongsTo('organizationType')
});