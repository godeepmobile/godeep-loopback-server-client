App.OrganizationType = DS.Model.extend({
	'name'          : DS.attr('string')
  , 'organizations' : DS.hasMany('organization')
  , 'conference'    : DS.hasMany('conference')
})