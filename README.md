# Godeep Mobile - LoopBack framework PoC, an Ember client which connects to it

This project is a "Proof of Concepts" app to check how the [LoopBack](http://loopback.io) framework behaves with a simple Ember client.

## Prerequisites

You will need the following things properly installed on your computer.

* [Git](http://git-scm.com/)
* [Node.js](http://nodejs.org/) (with NPM)
* [LoopBack](http://loopback.io)
* [Ember.js](http://emberjs.com/)

## Installation

* `git clone <repository-url>` this repository
* change into the new directory
* npm install express

## Running / Development

* `node server.js`
* Visit your app at [http://localhost:3010](http://localhost:3010).

## Further Reading / Useful Links

* [Ember](http://emberjs.com/api/)
* [Ember Data](http://emberjs.com/api/data/)
* [EmberDataModelMaker](http://andycrum.github.io/ember-data-model-maker/)